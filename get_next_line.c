/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 09:58:03 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/22 13:41:37 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
 #include "libft.h"
# define BUFF_SIZE 5000
int		get_next_line(const int fd, char **line)
{
	static char		*buff;
	static int		buff_i;
	static int		i;

	while((i = read(fd, buff,BUFF_SIZE)) != 0)
	{
		buff[i] = '\0';



}
int		check_newline(char *buff, int index)
{
	int i;

	i = index;
	while (buff[i])
	{
		if (buff[i] == '\n')
		{
			//i++;
			return (i);
		}
		i++;
	}
	return (-1);
}
int		get_line(const int fd/*, char **line*/)
{
	static char		*buff;
	static int		buff_i;
	char			*temp;
	static int		i;
	int				temp_i;

	buff_i = -1;
	while((i = read(fd, buff,BUFF_SIZE)) != 0)
	{
		buff[i] = '\0';
		//temp_i = buff_i;
		buff_i = check_newline(buff, ++buff_i);
		if (buff_i == -1)
		{
		   	if (line)
			{
				temp = line;
				*line = strjoin(strsub(buff, buff_i, i), temp);
			}
			else
				*line = strdup(buff);
		}
		else if (buff_i != -1)
		{
			temp_i = buff_i;
			if (line)
			{
				temp = line;
				*line = strjoin(strsub(buff, temp_i, i), temp);
			}
			else
				*line = strdup(buff);
		}




/*

#include "get_next_line.h"

int		get_lines(int fd, char **line, char **str)
{
	int		i;
	char	*temp;

	i = 0;
	while (str[fd][i] != '\n' && str[fd][i] != '\0')
		i++;
	if (str[fd][i] == '\0')
	{
		*line = ft_strdup(str[fd]);
		ft_strdel(&str[fd]);
	}
	else if (str[fd][i] == '\n')
	{
		*line = ft_strsub(str[fd], 0, i);
		temp = ft_strdup(&str[fd][i + 1]);
		free(str[fd]);
		str[fd] = temp;
	}
	return (1);
}

int		get_next_line(const int fd, char **line)
{
	static char		*str[1];
	int				ret;
	char			*temp;
	char			buffer[BUFF_SIZE + 1];

	if (fd < 0 || line == NULL)
		return (-1);
	while ((ret = read(fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[ret] = '\0';
		if (str[fd] == NULL)
			str[fd] = ft_strnew(1);
		temp = ft_strjoin(str[fd], buffer);
		free(str[fd]);
		str[fd] = temp;
	}
	if (ret < 0)
		return (-1);
	if (ret == 0 && (str[fd] == NULL || str[fd][0] == '\0'))
		return (0);
	return (get_lines(fd, line, str));
}
}

#include "get_next_line.h"
#include "./Libft/libft.h"
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
	int fd = open("get_next_line.c", O_RDONLY);
	int filde = open("main_gnl.c", O_RDONLY);
	int i;
	char	*line;
	i = 0;


	while (get_next_line(fd, &line) || get_next_line(filde, &line))
	{
		i++;
		printf("%s\n", line);
		free(line);
	}
	return (0);
}
*/


